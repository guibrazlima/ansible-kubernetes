# Kubernetes single-master or multi-master cluster on baremetal or openstack
## Contents:
* Example playbook and inventory files.
* Task to fetch credentials to the ansible runner machine.
* Manfifest to deploy on Openstack VMs or on any existing VMs.
* Manifests to deploy the dashboard, a load balancer, an ingress and rook-ceph.
* You can install a multi-master kubernetes cluster to be use in production environments.


## How to use:
* The use of [cloud-init][cloud-init] is suggested, create an **ansible user** with a **ssh key** on every machine.
* Use your hypervisor of choice or maybe the cloud and deploy at least **1 kube_master** and **1 kube_node**.
* If you use the tag instances, it will provision the virtual machines from inventory in openstack.
* Populate your ansible [inventory][hosts-ini] with hostnames and IPs of the machines (example below).
```
[all]
kube-master-01 ansible_host=10.1.0.10
kube-master-02 ansible_host=10.1.0.11
kube-master-03 ansible_host=10.1.0.12
kube-node-01   ansible_host=10.1.0.13
kube-node-02   ansible_host=10.1.0.14
kube-node-03   ansible_host=10.1.0.15

[kube_master]
kube-master-01
kube-master-02
kube-master-03

[kube_node]
kube-node-01
kube-node-02
kube-node-03
(...)
```

### Choose your install option, either:
* Install barebones:
  ```bash
  ansible-playbook site.yml -i hosts.ini
  ```

* Install fully featured on baremetal or VMs in cloud provider:
  ```bash
  ansible-playbook site.yml -i hosts.ini --tags 'bootstrap,master,nodes,metallb,ingress,rook-ceph,dashboard,tokens'
  ```
  
* Create all instances infrastructure needed for k8s cluster on Openstack, and next install a k8s cluster fully featured :
  ```bash
  ansible-playbook site.yml -i hosts.ini --tags 'instances,bootstrap,master,nodes,metallb,ingress,rook-ceph,dashboard,tokens'
  ```
  (requires changes on [`ansible/group_vars/kube_master.yml`][kube_master.yml] for [MetalLB][metallb-l2])
  (requires changes on [`ansible/group_vars/all.yml`][all.yml] for [rook-ceph][rook-ceph])
  


## ! WARNING !
This is intended for a testing/development cluster, there are no verifications of any sort.  
This deployment should be fine for a GitLab Runner.  
Security might be overlooked for convenience.  


## Resources:
Extended documentation ([docs/extras.md][docs-extras]).  

This is based on the official [kubeadm documentation][kubeadm-docs].  
The [Ubuntu Cloud 18.04 LTS][ubuntu-cloud] image was used.


[cloud-init]: https://cloudinit.readthedocs.io/en/latest/
[docs-extras]: docs/extras.md
[hosts-ini]: ansible/hosts.ini
[kube_master.yml]: ansible/group_vars/kube_master.yml
[all.yml]: ansible/group_vars/all.yml
[kubeadm-docs]: https://kubernetes.io/docs/reference/setup-tools/kubeadm/
[kubeadm-upgrade-docs]: https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-upgrade/
[metallb-l2]: https://metallb.universe.tf/configuration/#layer-2-configuration
[rook-ceph]: https://rook.io/docs/rook/v1.1/ceph-storage.html
[ubuntu-cloud]: https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img

